/*
** Single function for FLAG WIDGET
*/
function flag_widget(self_id,textbox_id){

	if(jQuery('#'+self_id).val()=='+44 ')
		jQuery("#"+textbox_id).mask("999 999 9999");
	else if(jQuery('#'+self_id).val()=='+1 ')
		jQuery("#"+textbox_id).mask("(999) 999-9999");
	else if(jQuery('#'+self_id).val()=='+27 ')
		jQuery("#"+textbox_id).mask("999-999-9999");
	else
		jQuery("#"+textbox_id).mask("(999) (999) 9999");
	
	jQuery("#"+textbox_id).focus();

	jQuery("#"+self_id).focus(function(){
		alert(textbox_id);
    jQuery("#"+textbox_id).focus();
  });
}